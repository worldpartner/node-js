const { autos_create_get } = require("../controllers/auto");

var Auto = function(id, color, modelo, ubicacion){          
          this.id = id;
          this.color = color;
          this.modelo = modelo;
          this.ubicacion = ubicacion;
}

Auto.prototype.toString= function(){
          return 'id: ' + this.id + "| color: " + this.color;
}

Auto.allAutos = [];
Auto.add = function(aAuto){
          Auto.allAutos.push(aAuto);
}


Auto.finById = function(aAutoId){
          var  aAuto = Auto.allAutos.find(x => x.id == aAutoId);
          if (aAuto)
                    return aAuto;
          else 
          throw new Error (`No existe un Auto con el Id: ${aAutoId}`)          
}       

Auto.removeById = function(aAutoId){
          
          for (var i = 0; i < Auto.allAutos.length; i++){
                    if( Auto.allAutos[i].id == aAutoId){
                              Auto.allAutos.splice(i,1);
                              break;
                    }

          }
}



/*var a = new Auto(1,'Azul', 'Todo Terreno', [52.522128, 13.387853]);
var b = new Auto(2,'Verde', 'Familiar', [52.520705, 13.388159]);

Auto.add(a);
Auto.add(b);*/

module.exports = Auto;