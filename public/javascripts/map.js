var map = L.map('main_map').setView([52.5259432,13.3931584], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(map);



$.ajax({
    dataType: "json", 
    url: "api/autos", 
    success: function(result){
        console.log(result);
        result.autos.forEach(function (auto) {
            L.marker(auto.ubicacion, {title: auto.id}).addTo(map);      
        });
     }    
})

