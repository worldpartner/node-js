var Auto = require('../modules/auto');

exports.auto_list = function(req, res){
          res.render('autos/index', {autos: Auto.allAutos});
}

exports.autos_create_get = function(req, res){

          res.render('autos/create');
}

exports.autos_create_post = function(req, res){
          var auto = new Auto(req.body.id, req.body.color, req.body.modelo);
          auto.ubicacion = [req.body.latitud, req.body.longitud];
          Auto.add(auto);

          res.redirect('/autos');
}

exports.autos_update_get = function(req, res){
          var auto = Auto.finById(req.param.id); // no se usa body por viene por parametro 
          res.render('autos/update' , {auto});
}

exports.autos_update_post = function(req, res){
          var auto = Auto.finById(req.param.id);
          auto.id = req.body.id;
          auto.color = req.body.color;
          auto.modelo = req.body.modelo;
          auto.ubicacion = [req.body.latitud, req.body.longitud];
          

          res.redirect('/autos');
}

exports.autos_delete_post = function(req, res){
          Auto.removeById(req.body.id);
          res.redirect('/autos')
}

