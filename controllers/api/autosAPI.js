var Auto = require('../../modules/auto');

exports.auto_list = function (req, res){
          res.status(200).json({
                    autos: Auto.allAutos
          });

}

exports.auto_create = function(req , res){
          var auto = new Auto(req.body.id, req.body.color, req.body.modelo);
          auto.ubicacion = [req.body.latitud, req.body.longitud ];

          Auto.add(auto);

          res.status(200).json({
                    autos: Auto.allAutos
          })
}

exports.auto_delete = function(req, res){
          Auto.removeById(req.body.id);
          res.status(204).send();
}

exports.autos_update = function(req, res){
          var auto = Auto.finById(req.body.id);
          auto.id = req.body.id;
          auto.color = req.body.color;
          auto.modelo = req.body.modelo;
          auto.ubicacion = [req.body.latitud, req.body.longitud];
          

          res.status(200);
}