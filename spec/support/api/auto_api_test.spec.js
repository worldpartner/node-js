var Auto = require('../../../modules/auto');
var request = require('request');
var server = require('../../../bin/www');

beforeEach(function() {console.log("'testeando...'")});

describe('Autos Api', () =>{
          describe('GET AUTOS /', () =>{
                    it ('Status 200', () =>{
                              expect(Auto.allAutos.length).toBe(0);
                              var a = new Auto(1,'Azul', 'Todo Terreno', [52.522128, 13.387853]);
                              Auto.add(a);

                              request.get('http://localhost:3000/api/Autos', function(error, response , body){
                                        expect(response.statusCode).toBe(200);

                              });
                    });
          });

});

describe('POST AUTOS /create', () =>{
          it('STATUS 200', (done) =>{
                    var headers = {'content-type' : 'application/json'};
                    var a = '{"id": 10, "color": "Azul", "modelo": "urbano", "latitud": "92839", "longitud": "308292"}';
                    request.post({
                              headers: headers,
                              url: 'http://localhost:3000/api/autos/create',
                              body: a
                    }, function(error, response, body){
                              expect(response.statusCode).toBe(200);
                              expect(Auto.finById(10).color).toBe("Azul");
                              done();
                    });
          });

});