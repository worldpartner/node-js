var Auto = require('../../../modules/auto');

beforeEach(() => {Auto.allAutos = []; }); // esto hace que se ejecute la funcion indicada al inicio de cada spec o 

describe('Auto.allAutos', ()=> {
          it('comienza vacia', () => {
          expect(Auto.allAutos.length).toBe(0);
          });
});

describe('Auto.add', ()=> {
          it('Agregando uno', () => {
          expect(Auto.allAutos.length).toBe(0);
          
          var a = new Auto(1,'Azul', 'Todo Terreno', [52.522128, 13.387853]);
          Auto.add(a)

          expect(Auto.allAutos.length).toBe(1);
          expect(Auto.allAutos[0]).toBe(a);
          });
});

describe('Auto.findById', ()=> {
          it('Devuelve el Auto con Id 1', () => {
          expect(Auto.allAutos.length).toBe(0);
          
          var a = new Auto(1,'verde', 'Todo Terreno');
          var b = new Auto(2,'Rojo', 'Urbano');
          Auto.add(a);
          Auto.add(b);

          var targetAuto = Auto.finById(1);

          expect(targetAuto.id).toBe(1);
          expect(targetAuto.color).toBe(a.color);
          expect(targetAuto.modelo).toBe(a.modelo)
          });
});