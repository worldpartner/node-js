var express = require('express');
var router = express.Router();
var autoController = require('../controllers/auto');

router.get('/', autoController.auto_list);
router.get('/create', autoController.autos_create_get);
router.post('/create', autoController.autos_create_post);
router.get('/:id/update', autoController.autos_update_get);
router.post('/:id/update', autoController.autos_update_post);
router.post('/:id/delete', autoController.autos_delete_post); // al indicar :id se espera recibir un parametro 

module.exports = router;
