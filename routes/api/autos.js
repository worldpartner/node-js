var express = require('express');
var router = express.Router();
var autoController = require('../../controllers/api/autosAPI');

router.get('/', autoController.auto_list);
router.post('/create', autoController.auto_create);
router.delete('/delete', autoController.auto_delete);
router.post('/update', autoController.autos_update);

module.exports = router;